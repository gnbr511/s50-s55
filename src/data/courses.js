const courses_data = [
	{
		id: "wd001",
		name: "PHP and Laravel",
		description: "The quick brown fox jumps over the lazy dog.",
		price: 40000,
		onOffer: true
	},
	{
		id: "wd002",
		name: "Python and Django",
		description: "The quick brown fox jumps over the lazy dog.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wd003",
		name: "Java and Springboot",
		description: "The quick brown fox jumps over the lazy dog.",
		price: 70000,
		onOffer: true
	}
]

export default courses_data