import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const navigate = useNavigate()
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		(email !== '' && password !== '') ?
		setIsActive(true):setIsActive(false)
	}, [email, password])

	const retrieveUser = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
		})
	}

	function authenticate(event){
		event.preventDefault()		
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.accessToken !== "undefined"){
				localStorage.setItem('token', result.accessToken)
				retrieveUser(result.accessToken)
				Swal.fire({
					title: 'Login Successful!',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			}else{
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Pasensya ka na, sa kathang isip kong ito'
				})	
			}
		})
	}

	return(
			(user.id !== null) ?
				<Navigate to="/courses"/>
			:
				<Form onSubmit={event => authenticate(event)}>
					<Form.Group>
						<Form.Label>Email</Form.Label>
						<Form.Control 
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={event => setEmail(event.target.value)}
							required
						/>
						<Form.Text className="text-muted">
							 We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password"
							placeholder="Enter password"
							value={password}
							onChange={event => setPassword(event.target.value)}
							required
						/>
					</Form.Group>
					{
						isActive
						?
						<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
						:
						<Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
					}
				</Form>

	)
}