import {Row, Col, Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

export default function CourseCard({course}) {
	//destructuring the props
	const {name, description, price, _id} = course

	//Using the state
	const [enrollees, setEnrollees] = useState(0)
	const [slots, setSlots] = useState(15)
	const [isOpen, setIsOpen] = useState(true)

	// function enroll(){
	// 	if(slots>0){
	// 		setSlots(slots - 1)
	// 		setEnrollees(enrollees + 1)
	// 	}else{
	// 		alert("No more slots!")
	// 	}		
	// }

	// useEffect(() => {
	// 	if(slots === 0){
	// 		setIsOpen(false)
	// 	}
	// }, [slots])
	return(
		<Row>
			<Col>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>
						<Card.Text>Slots: {slots}</Card.Text>
						<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
					</Card.Body>
				</Card>
			</Col>			
		</Row>
	)
}

CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}